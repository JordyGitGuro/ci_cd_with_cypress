// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })

Cypress.Commands.add('openAsisim', (value) => {
    cy.visit("http://localhost:9000/")
})

Cypress.Commands.add("isVisible", (value) => {
    cy.get(value).should('be.visible')
})

Cypress.Commands.add("isInvisible", (value) => {
    cy.get(value).should('not.be.visible')
})

Cypress.Commands.add("login", (username, password) => {
    cy.get("[name = 'username']").type(username)
    cy.get("[name = 'password']").type(password)
    cy.get(".login-button").click()
    cy.isVisible(".navbar-nav.pull-left")
})

Cypress.Commands.add("soldierTreatment", (value) => {
    cy.get("#SoldierTreatment").select(value)
})

Cypress.Commands.add("refrences", () => {
    cy.get("[href='/references/all']").click()
})

Cypress.Commands.add("presciption", () => {
    cy.get("[href='/prescriptions/all']").click()
})

Cypress.Commands.add("appointment", (value) => {
    cy.get(".dropdown-toggle").contaions("תורים").select(value)
})

Cypress.Commands.add("clinic", (value) => {
    cy.get(".dropdown-toggle").contaions(" ניהול מרפאה ").select(value)
})

Cypress.Commands.add("BasisManagement", () => {
    cy.get("#BasisManagement").click()
})

Cypress.Commands.add("logOut", () => {
    cy.get(".nav.navbar-nav.pull-left").click()
})

// delay coomand for spesific commands 
// const COMMAND_DELAY = 1200;


// for (const command of ['clear']) {
//     Cypress.Commands.overwrite(command, (originalFn, ...args) => {
//         const origVal = originalFn(...args);

//         return new Promise((resolve) => {
//             setTimeout(() => {
//                 resolve(origVal);
//             }, COMMAND_DELAY);
//         });
//     });
// } 

