/// <reference types="cypress" />

describe('', () => {
    it('locating', () => {
        // find - find whats inside the elemnt
        cy
        .get("[name='q']")
        .find('.cls')

        // children - get all the children elemnts 
        cy
        .get('nav')
        .children()     
        // can be with selection
        cy
        .get('ul')
        .children('li')

        // eq -get element by index
        cy
        .get('li')
        .eq(5)

        // first - get first element
        cy
        .get('li')
        .first()

        // last - get last element
        cy
        .get('li')
        .last()

        // next - get next element
        cy
        .get('li')
        .first()
        .next()

        // prev - get previoes element
        cy
        .get('li')
        .last()
        .prev()

        // not - filter whats not 
        cy
        .get('li')
        .not('.asia')

        // parent - get parent
        cy
        .get('li')
        .parent()

        // filter - another loactor
        cy
        .get('li')
        .filter('input')
      
    });
});