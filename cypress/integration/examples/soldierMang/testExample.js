/// <reference types="cypress" />

describe('suite', () => {
    before(() => {
        cy.fixture('googleSearches.json').as('searchData');
        cy.visit('https://www.google.co.il/?gws_rd=ssl')
        cy.get(".a4bIc").type("first");
        cy.get('.aajZCb [value="Google Search"]').click();
    });

    
    it('multi search', function() {
        const searches = this.searchData;
    
            searches.googleSearches.forEach(valueToSearch => {
            cy.get(".a4bIc").type("{selectAll}{backspace}");
            cy.get(".a4bIc").type(valueToSearch);
            cy.get('.Tg7LZd').click();
            
        });
    });
});