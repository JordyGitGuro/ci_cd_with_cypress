import Login from '../PageObject/Login'

describe('Login', () => {
    const login = new Login()
    let negData

    before(() => {
        cy.fixture('LoginNegativeTesting.json').then((data) => {
        negData = data;
        })
    })
    
    beforeEach(() => {
        cy.openAsisim()
    })

    it('Sign in as manager', () => {
        cy.login('manager','manager')
    })

    it('Sign in as clerk', () => {
        cy.login('clerk','clerk')
    })

    it('Sign in as developer', () => {
        cy.login('developer','developer')
    })

    it('Negative Testing', () => {
        negData.loginData.forEach((value) => {
            if (value.username) {
                login.enterUsername(value.username)
            }

            if (value.password) {
                login.enterPassword(value.password)
            }

        login.clickLoginButton()
        login.clearFields()
        login.mainPageElementShouldNotExist()
        })
    })
})