import AddSoldier from "../PageObject/AddSoldier"

describe('Soldier Mangement', () => {
    const addSoldier = new AddSoldier()
    let numOfSoldiersBefore;
    const name = 'Yardii'
    const personalNumber = '1234865' 
    let negData

    before(() => {
        cy.fixture('SoldierNegativeTesting.json').then((data) => {
        negData = data;
        })
    })

    beforeEach(() => {
        cy.openAsisim()
        cy.login("developer","developer")
        cy.BasisManagement()
        addSoldier.clickSoldierManagement()
    })

    it('Negative Testing', () => {
        cy.get("#table > tr").its("length").then(value => {
            numOfSoldiersBefore = value;
        })

        negData.SoldierDetails.forEach((value) => {
            addSoldier.clickNewSoldierButton()
            if (value.personalNumber) {
                addSoldier.enterNewId(value.personalNumber)
            }

            if (value.name) {
                addSoldier.enterNewName(value.name)
            }
     
            if (value.clinic) {
                addSoldier.selectNewClinicId(value.clinic)
            }
            
            if (value.draftingDay) {
                addSoldier.enterNewDraftDate(value.draftingDay)
            }

            if (value.releaseDay) {
                addSoldier.enterNewReleaseDate(value.releaseDay)
            }

            if (value.rank) {
                addSoldier.selectNewRankId(value.rank)
            }
            addSoldier.clickCreateButton()
            addSoldier.checkIfFailed()
            addSoldier.clickConfirmButton()
        })
    }) 

    it('Adding a new soldier to the system', () => {
        cy.get("#table > tr").its("length").then(value => {
            numOfSoldiersBefore = value;
        });

        addSoldier.clickNewSoldierButton()
        addSoldier.enterNewId(personalNumber)
        addSoldier.enterNewName(name)
        addSoldier.selectNewClinicId('City Officer - Center')
        addSoldier.enterNewDraftDate("2018-08-07")
        addSoldier.enterNewReleaseDate("2020-08-07")

        // Selecting a random option 
        cy.get("#newRankId").then((lol) => {
        var optionVal = new Array();
        optionVal = lol.children()
        var shit = optionVal[Math.floor(Math.random() * optionVal.length) + 1]
        addSoldier.selectNewRankId(shit.text)
        })
        
        addSoldier.clickCreateButton()
        addSoldier.checkIfSuccess()
        addSoldier.clickConfirmButton()
    })
})
