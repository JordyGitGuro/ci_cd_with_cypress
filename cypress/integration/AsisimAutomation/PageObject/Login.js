class Login {
    enterUsername(value) {
        cy.get("[name = 'username']").type(value)
    }

    enterPassword(value) {
        cy.get("[name = 'password']").type(value)
    }

    clickLoginButton() {
       cy.get(".login-button").click()
    }

    clearFields() {
        cy.get("[name = 'username']").clear()
        cy.get("[name = 'password']").clear()
    }

    mainPageElementShouldExist() {
        cy.isVisible(".navbar-nav.pull-left")
    }

    mainPageElementShouldNotExist() {
        cy.isInvisible(".navbar-nav.pull-left")
    }

}
export default Login
