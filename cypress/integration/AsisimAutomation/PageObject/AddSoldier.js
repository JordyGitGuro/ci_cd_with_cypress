class AddSoldier {
    clickSoldierManagement() {
        cy.get("[href='/manage/soldiers']").click()
    }

    clickNewSoldierButton() {
        cy.get("[type='submit']").contains("חדש").click()
    }

    enterNewId(value) {
        cy.get('#newId').click().clear().type(value).should('have.value', value);
    }

    enterNewName(value) {
        cy.get('#newName').clear().type(value).should('have.value', value);
    }

    selectNewClinicId(value) {
        cy.get("#newClinicId").select(value)
    }

    enterNewDraftDate(value) {
        cy.get("#newDraftDate").clear().type(value)
    }

    enterNewReleaseDate(value) {
        cy.get("#newReleaseDate").clear().type(value)
    }

    selectNewRankId(value) {
        cy.get("#newRankId").select(value)
    }

    clickCreateButton() {
        cy.get(".btn").contains("צור").click()
    }

    clickConfirmButton() {
        cy.get(".confirm").click()
    }

    checkIfFailed() {
        cy.get(".animateErrorIcon").should('be.visible')
    }

    checkIfSuccess() {
        cy.get(".success.animate").should('be.visible')
    }
}
export default AddSoldier
